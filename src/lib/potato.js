// our libs
import { sumAndDiff } from './hashingFunctions';
import { allPaths }       from './imageFiles';
import SlotMachine      from './slotMachine';

const colors = [
  '#1976d2',
  '#7e57c2',
  '#7cb342',
  '#e53935',
  '#bdbdbd',
  '#4db6ac',
  '#ffeb3b',
  '#ec407a',
  '#ffca28',
];

class Potato {
  constructor() {
    this.colorMachine = new SlotMachine(colors);
    this.eyesMachine  = new SlotMachine(allPaths('eyes'));
    this.mouthMachine = new SlotMachine(allPaths('mouth'), sumAndDiff);
  }

  // Construct Faces Parts
  parts(string) {
    return {
      color : this.colorMachine.pull(string),
      eyes  : this.eyesMachine.pull(string),
      mouth : this.mouthMachine.pull(string),
    };
  }
}

export default new Potato();
